import re
import string
from functools import cmp_to_key
from itertools import groupby

from utils import read_inputs


class Room:
    def __init__(self, name, sector, checksum):
        self.name = name
        self.sector = int(sector)
        self.checksum = checksum

    @staticmethod
    def parse(raw):
        pattern = r'([a-z-]+)(\d+)\[(\w+)\]'
        parsed = re.findall(pattern, raw)[0]

        return Room(name=parsed[0], sector=parsed[1], checksum=parsed[2])


def count_frequencies(input, ignored=['-']):
    return {key: len(list(values)) for key, values in groupby(sorted(input)) if key not in ignored}


def sort_frequencies(frequencies):
    def alphasort(x1, x2):
        (a, i) = x1
        (b, j) = x2

        if i != j:
            return j - i

        return ord(a) - ord(b)

    return sorted(frequencies.items(), key=cmp_to_key(alphasort))


def run_checksum(name):
    frequencies = count_frequencies(name)
    sorted_frequencies = sort_frequencies(frequencies)

    return ''.join([c for (c, n) in sorted_frequencies[:5]])


def caesar_cipher(plaintext, shift):
    alphabet = string.ascii_lowercase
    shift = shift % len(alphabet)
    shifted_alphabet = alphabet[shift:] + alphabet[:shift]
    table = str.maketrans(alphabet, shifted_alphabet)

    return plaintext.translate(table).replace('-', ' ')


if __name__ == '__main__':
    valid_rooms = [room for room
                   in map(Room.parse, read_inputs("day4").splitlines())
                   if room.checksum == run_checksum(room.name)]

    print(sum([room.sector for room in valid_rooms]))
    print([(caesar_cipher(room.name, room.sector), room.sector) for room in valid_rooms])
