def read_inputs(name):
    with open("inputs/" + name + ".txt") as f:
        return f.read()
