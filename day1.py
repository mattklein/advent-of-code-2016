from utils import read_inputs


def taxicab_distance(point):
    return abs(point.real) + abs(point.imag)


if __name__ == '__main__':
    instructions = read_inputs("day1").split(', ')

    position = 0 + 0j
    direction = 0 + 1j
    visited = [position]
    solved = False

    for instruction in instructions:
        rotation = instruction[0]
        delta = int(instruction[1:])

        if rotation == 'L':
            direction *= 1j
        elif rotation == 'R':
            direction *= -1j

        for _ in range(delta):  # we need to detect when we first enter a duplicate block
            position += direction

            if position in visited and not solved:
                print('Duplicate distance = {}'.format(taxicab_distance(position)))
                solved = True

            visited += [position]

    print('Total distance = {}'.format(taxicab_distance(position)))
