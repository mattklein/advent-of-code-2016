import heapq
from collections import Counter
from operator import itemgetter

from utils import read_inputs


def least_common(counter, to_find=None):
    if to_find is None:
        return sorted(counter.items(), key=itemgetter(1), reverse=False)

    return heapq.nsmallest(to_find, counter.items(), key=itemgetter(1))


if __name__ == '__main__':
    messages = read_inputs("day6").splitlines()
    common_length = len(messages[0])

    solution1 = []
    solution2 = []

    for i in range(0, common_length):
        column = [line[i] for line in messages]

        counter = Counter(column)

        key1, _ = counter.most_common()[0]
        key2, _ = least_common(counter)[0]

        solution1.append(key1)
        solution2.append(key2)

    print('The most common occuring characters = {}'.format(''.join(solution1)))
    print('The least common occuring characters = {}'.format(''.join(solution2)))
