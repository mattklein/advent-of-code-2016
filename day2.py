from numpy.core.fromnumeric import clip

from utils import read_inputs


def calculate_code(numberpad, origin):
    def get_digit(point):
        return numberpad[point[1]][point[0]]

    code = []
    current_point = list(origin)

    for line in instructions:
        for instruction in line:
            new_point = list(current_point)

            if instruction == 'R':
                new_point[0] += 1
            if instruction == 'L':
                new_point[0] -= 1
            if instruction == 'U':
                new_point[1] -= 1
            if instruction == 'D':
                new_point[1] += 1

            new_point = clip(new_point, 0, len(numberpad) - 1)

            if get_digit(new_point) != 'X':
                current_point = list(new_point)

        code += [get_digit(current_point)]

    return code


if __name__ == '__main__':
    instructions = read_inputs("day2").splitlines()

    code1 = calculate_code(
        numberpad=[['1', '2', '3'],
                   ['4', '5', '6'],
                   ['7', '8', '9']],
        origin=[1, 1],
    )

    print("The resultant code is {}".format(''.join(code1)))

    code2 = calculate_code(
        numberpad=[['X', 'X', '1', 'X', 'X'],
                   ['X', '2', '3', '4', 'X'],
                   ['5', '6', '7', '8', '9'],
                   ['X', 'A', 'B', 'C', 'X'],
                   ['X', 'X', 'D', 'X', 'X']],
        origin=[0, 2],
    )

    print("The resultant code is {}".format(''.join(code2)))
