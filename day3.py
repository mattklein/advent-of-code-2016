import numpy as np

from utils import read_inputs


def extract_triangle(string):
    return np.array([int(s) for s in string.split() if s.isdigit()])


def is_valid_triangle(triangle):
    a, b, c = triangle

    return a < b + c and \
           b < a + c and \
           c < a + b


def group(iterable, n=3):
    return zip(*(iter(iterable),) * n)


if __name__ == '__main__':
    triangles1 = np.array(map(extract_triangle, read_inputs("day3").splitlines()))
    triangles2 = triangles1.transpose()

    valid_triangles1 = [t for t in triangles1 if is_valid_triangle(t)]
    valid_triangles2 = []

    for column in map(group, triangles2):
        valid_triangles2 += [t for t in column if is_valid_triangle(t)]

    print("The number of valid trianlges is {}".format(len(valid_triangles1)))
    print("The number of valid triangles is {}".format(len(valid_triangles2)))
