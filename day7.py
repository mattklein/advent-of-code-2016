import re

from utils import read_inputs


class IPv7:
    def __init__(self, ip1, ip2, hypernet):
        self.ip1 = ip1
        self.ip2 = ip2
        self.hypernet = hypernet

    def supports_tls(self):
        def abba(x):
            return any(a == d and b == c and a != b for a, b, c, d in zip(x, x[1:], x[2:], x[3:]))

        return abba(self.ip1 + ' ' + self.ip2) and not abba(self.hypernet)

    @staticmethod
    def parse(string):
        components = re.split(r'\[([^\]]+)\]', string)

        return IPv7(ip1=components[0],
                    ip2=components[2],
                    hypernet=components[1])


if __name__ == '__main__':
    ips = map(IPv7.parse, read_inputs("day7").splitlines())
    supports_tls = filter(IPv7.supports_tls, ips)

    print('The number of IPs which support TLS is = {}'.format(len(supports_tls)))
