import hashlib
import sys

from utils import read_inputs

if __name__ == '__main__':
    door_id = read_inputs("day5")

    password1 = []
    password2 = ['?' for _ in range(0, 8)]

    for i in iter(range(0, sys.maxsize)):  # super slow
        next_entry = door_id + str(i)
        digest = hashlib.md5(next_entry.encode('utf-8')).hexdigest()

        if digest.startswith("00000"):
            if len(password1) < 8:
                password1 += digest[5]

            if digest[5].isdigit():  # ignore invalid positions
                position = int(digest[5])
                if 0 <= position < len(password2) and password2[position] is '?':  # only use the first result
                    password2[position] = digest[6]
                    print(''.join(password2))

        if len(password1) == 8 and all(_ is not '?' for _ in password2):
            break

    print('First password = {}'.format(password1))
    print('Second password = {}'.format(password2))
